package com.e.commerce.Schema;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class PerishableTest {

    @Test
    void shouldCreatePerishableItemWithId() {
        Perishable item = new Perishable(1, "Bread", 30, "Food", "2020-02-01", "2020-08-01");

        Integer id = item.getId();
        String itemName = item.getName();
        Integer price = item.getPrice();
        String type = item.getType();
        String manufacturingDate = item.getManufacturingDate();
        String expiryDate = item.getExpiryDate();
        String insertIntoDbCommand = item.insertIntoDb(1);

        assertEquals(1, id);
        assertEquals("Bread", itemName);
        assertEquals(30, price);
        assertEquals("Food", type);
        assertEquals("2020-02-01", manufacturingDate);
        assertEquals("2020-08-01", expiryDate);
        assertEquals("INSERT IGNORE INTO perishable (name, price, type, manufacturing_date, expiry_date) VALUES ('Bread', 30, 1, '2020-02-01', '2020-08-01');",
                insertIntoDbCommand);

    }

    @Test
    void shouldCreatePerishableItemWithoutId() {
        Perishable item = new Perishable( "Bread", 30, "Food", "2020-02-01", "2020-08-01");

        String itemName = item.getName();
        Integer price = item.getPrice();
        String type = item.getType();
        String manufacturingDate = item.getManufacturingDate();
        String expiryDate = item.getExpiryDate();
        String insertIntoDbCommand = item.insertIntoDb(1);

        assertEquals("Bread", itemName);
        assertEquals(30, price);
        assertEquals("Food", type);
        assertEquals("2020-02-01", manufacturingDate);
        assertEquals("2020-08-01", expiryDate);
        assertEquals("INSERT IGNORE INTO perishable (name, price, type, manufacturing_date, expiry_date) VALUES ('Bread', 30, 1, '2020-02-01', '2020-08-01');",
                insertIntoDbCommand);
    }


}

