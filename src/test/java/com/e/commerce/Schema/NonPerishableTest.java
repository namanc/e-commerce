package com.e.commerce.Schema;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NonPerishableTest {

    @Test
    void shouldCreateNonPerishableItemWithGivenId() {
        NonPerishable item = new NonPerishable(1, "Chair", 1300, "Luxury", "2020-02-01");

        Integer id = item.getId();
        String itemName = item.getName();
        Integer price = item.getPrice();
        String type = item.getType();
        String manufacturingDate = item.getManufacturingDate();
        String insertIntoDbCommand = item.insertIntoDb(2);

        assertEquals(1, id);
        assertEquals("Chair", itemName);
        assertEquals(1300, price);
        assertEquals("Luxury", type);
        assertEquals("2020-02-01", manufacturingDate);
        assertEquals("INSERT IGNORE INTO non_perishable (name, price, type, manufacturing_date) VALUES ('Chair', 1300, 2, '2020-02-01');",
                insertIntoDbCommand);
    }

    @Test
    void shouldCreateNonPerishableItemWithoutId() {
        NonPerishable item = new NonPerishable("Chair", 1300, "Luxury",  "2020-02-01");

        String itemName = item.getName();
        Integer price = item.getPrice();
        String type = item.getType();
        String manufacturingDate = item.getManufacturingDate();
        String insertIntoDbCommand = item.insertIntoDb(2);

        assertEquals("Chair", itemName);
        assertEquals(1300, price);
        assertEquals("Luxury", type);
        assertEquals("2020-02-01", manufacturingDate);
        assertEquals("INSERT IGNORE INTO non_perishable (name, price, type, manufacturing_date) VALUES ('Chair', 1300, 2, '2020-02-01');",
                insertIntoDbCommand);
    }

}