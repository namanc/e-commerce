package com.e.commerce.Schema;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CategoryTest {
    @Test
    void shouldCreateCategoryWithGivenID() throws InvalidInputException {

        Category category = new Category(5, "Food", 18);

        Integer id = category.getId();
        String type = category.getName();
        Integer gst = category.getGst();

        assertEquals(5, id);
        assertEquals("Food", type);
        assertEquals(18, gst);

    }

    @Test
    void shouldCreateCategoryWithNullId() throws InvalidInputException {
        Category category = new Category(-1, "Food", 18);

        Integer id = category.getId();
        String type = category.getName();
        Integer gst = category.getGst();

        assertNull(id);
        assertEquals("Food", type);
        assertEquals(18, gst);
    }

    @Test
    void shouldNotCreateCategoryWithNegativeGST() throws InvalidInputException {
        try{
            Category category = new Category(1, "Food", -18);
            fail();
        } catch (InvalidInputException e){
            assertEquals("Invalid Input for : GST Can't be : Negative", e.getMessage());
        }

    }

    @Test
    void shouldCreateCategoryWithoutId() throws InvalidInputException {
        Category category = new Category("Food", 18);

        Integer id = category.getId();
        String type = category.getName();
        Integer gst = category.getGst();

        assertNull(id);
        assertEquals("Food", type);
        assertEquals(18, gst);
    }
}