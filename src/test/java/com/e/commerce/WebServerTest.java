package com.e.commerce;

import com.e.commerce.Schema.NonPerishable;
import com.e.commerce.Schema.Perishable;
import com.google.gson.Gson;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

class WebServerTest {
    private WebServer webServer;
    private String url = "http://localhost:80";

    @BeforeEach
    void setUp () throws IOException {
        this.webServer = new WebServer();

        webServer.startServer();
    }

    @AfterEach
    void stopServer () {
        this.webServer.stopServer();
    }

    @Test
    void shouldAddPerishableProductOnServer() {
        Gson data = new Gson();
        String inputData = data.toJson(new Perishable("Bread", 30,"Food", "2020-02-01", "2020-08-01"));

        given()
                .baseUri(url)
                .body(inputData)
                .when()
                .post("/add-new-perishable-product")
                .then()
                .statusCode(200)
                .body(equalTo("Product added to Server (DB)"));
    }

    @Test
    void shouldNotAddPerishableProductWithIncorrectSchema() {
        Gson data = new Gson();
        String inputData = data.toJson(new Perishable(null, 32, "Food", null, null));

        given()
                .baseUri(url)
                .body(inputData)
                .when()
                .post("/add-new-perishable-product")
                .then()
                .body(equalTo("Invalid Schema for perishable product!"));
    }

    @Test
    void shouldAddNonPerishableProductOnServer() {
        Gson data = new Gson();
        String inputData = data.toJson(new NonPerishable("Chair", 1300, "Luxury", "2020-02-01"));

        given()
                .baseUri(url)
                .body(inputData)
                .when()
                .post("/add-new-non-perishable-product")
                .then()
                .statusCode(200)
                .body(equalTo("Product added to Server (DB)"));

    }

    @Test
    void shouldNotAddNonPerishableProductWithIncorrectSchema() {
        Gson data = new Gson();
        String inputData = data.toJson(new NonPerishable(null, 133, null, null));

        given()
                .baseUri(url)
                .body(inputData)
                .when()
                .post("/add-new-non-perishable-product")
                .then()
                .body(equalTo("Invalid Schema for non-perishable product!"));
    }

    @Test
    void shouldAddPerishableItemToCart() {
        Gson data = new Gson();
        String inputData = data.toJson(1);

        given()
                .baseUri(url)
                .body(inputData)
                .when()
                .post("/add-perishable-item-to-cart")
                .then()
                .body(equalTo("Item added to cart!"));
    }

    @Test
    void shouldAddNonPerishableItemToCart() {
        Gson data = new Gson();
        String inputData = data.toJson(1);

        given()
                .baseUri(url)
                .body(inputData)
                .when()
                .post("/add-non-perishable-item-to-cart")
                .then()
                .body(equalTo("Item added to cart!"));

    }

    @Test
    void shouldViewCart() {
        Gson data = new Gson();
        String inputData = data.toJson(1);

        given()
                .baseUri(url)
                .body(inputData)
                .when()
                .post("/add-non-perishable-item-to-cart")
                .then()
                .body(equalTo("Item added to cart!"));

        given()
                .baseUri(url)
                .body(inputData)
                .when()
                .post("/add-perishable-item-to-cart")
                .then()
                .body(equalTo("Item added to cart!"));

        given()
                .baseUri(url)
                .body(inputData)
                .when()
                .post("/view-cart")
                .then()
                .body(equalTo("[{\"id\":1,\"price\":1300,\"name\":\"Chair\",\"type\":\"7\",\"manufacturingDate\":\"2020-02-01\"},{\"id\":1,\"name\":\"Bread\",\"type\":\"1\",\"manufacturingDate\":\"2020-02-01\",\"expiryDate\":\"2020-08-01\",\"price\":30}]"));


    }

    @Test
    void shouldCalculateGSTOfCart() {
        Gson data = new Gson();
        String inputData = data.toJson(1);

        given()
                .baseUri(url)
                .body(inputData)
                .when()
                .post("/add-non-perishable-item-to-cart")
                .then()
                .body(equalTo("Item added to cart!"));

        given()
                .baseUri(url)
                .body(inputData)
                .when()
                .post("/add-non-perishable-item-to-cart")
                .then()
                .body(equalTo("Item added to cart!"));


        given()
                .baseUri(url)
                .body(inputData)
                .when()
                .post("/add-perishable-item-to-cart")
                .then()
                .body(equalTo("Item added to cart!"));

        // Two chairs and one Bread

        given()
                .baseUri(url)
                .body(inputData)
                .when()
                .post("/GST-of-cart")
                .then()
                .body(equalTo("GST is : 261.5"));

    }
}