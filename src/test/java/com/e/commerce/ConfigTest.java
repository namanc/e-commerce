package com.e.commerce;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConfigTest {
    @Test
    void shouldReadUserNameFromConfigFile() {
        Config config = new Config("config.cfg");

        String user = config.getProperty("mDbUser");

        assertEquals("root", user);

    }
}