package com.e.commerce;

import com.e.commerce.Schema.Item;
import com.e.commerce.Schema.Perishable;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class DatabaseTest {

    @Test
    void shouldInsertItemIntoDB() {
        Database db = new Database(new Config("config.cfg"));

        Boolean bool = db.insertItem(new Perishable(1, "Bread", 30, "Food", "2020-02-01", "2020-08-01"));

        assertTrue(bool);
    }

    @Test
    void shouldGetDbName() {
        Database db = new Database(new Config("config.cfg"));

        String dbName = db.getDbName();
        Config cfg = new Config("config.cfg");

        assertEquals(cfg.getProperty("mDbName"), dbName);
    }

    @Test
    void shouldInsertRecordInCategoryTable() {
        Database db = new Database(new Config("config.cfg"));

        Integer id = db.insertType("Food", 0);

        assertEquals(1, id);

    }

    @Test
    void shouldGetTypeFromId() throws SQLException {
        Database db = new Database(new Config("config.cfg"));
        db.insertType("Food", 0);

        String type = db.getTypeFromId(1);

        assertNotNull(type);
    }

    @Test
    void shouldNotGetItemForWrongId() throws SQLException {
        Database db = new Database(new Config("config.cfg"));

        Item item = db.getItem(new Perishable());

        assertNull(item.getName());
        assertNull(item.getId());
    }

    @Test
    void shouldGetItemById() throws SQLException {
        Database db = new Database(new Config("config.cfg"));

        Item item = db.getPerishableItemById(1);

        assertNotNull(item.getName());
        assertNotNull(item.getId());

    }

    @Test
    void shouldGetGSTById() throws SQLException {
        Database db = new Database(new Config("config.cfg"));

        Integer gst = db.getGSTFromId(1);
        assertEquals(5, gst);

        gst = db.getGSTFromId(7);
        assertEquals(10, gst);

        gst = db.getGSTFromId(62);
        assertEquals(5, gst);
    }
}

