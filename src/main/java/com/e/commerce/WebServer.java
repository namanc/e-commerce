package com.e.commerce;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import com.e.commerce.Schema.Item;
import com.e.commerce.Schema.NonPerishable;
import com.e.commerce.Schema.Perishable;
import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class WebServer {

    private HttpServer httpServer;
    private ECommerce eCommerce;
    private ArrayList<Item> cart = new ArrayList<>();

    public void startServer() throws IOException {
        this.httpServer = HttpServer.create(new InetSocketAddress(80), 0);
        httpServer.createContext("/add-new-perishable-product", new addNewPerishableProduct());
        httpServer.createContext("/add-new-non-perishable-product", new addNewNonPerishableProduct());
        httpServer.createContext("/add-perishable-item-to-cart", new addPerishableItemToCart());
        httpServer.createContext("/add-non-perishable-item-to-cart", new addNonPerishableItemToCart());
        httpServer.createContext("/view-cart", new viewCart());
        httpServer.createContext("/GST-of-cart", new GSTOfCart());


        this.eCommerce = new ECommerce();

        httpServer.start();
    }

    public void stopServer() {
        httpServer.stop(0);
    }

    private String getJsonString(HttpExchange exchange) {
        return new Scanner(exchange.getRequestBody()).nextLine();
    }


    private void sendResponse(HttpExchange httpExchange, String responseToSend) throws IOException {
        httpExchange.sendResponseHeaders(200, responseToSend.length());
        OutputStream os = httpExchange.getResponseBody();
        os.write(responseToSend.getBytes());
        os.close();
    }

    class viewCart implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            Gson gson = new Gson();
            sendResponse(httpExchange, gson.toJson(cart));
        }

    }

    private class addNewPerishableProduct implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            try {
                Gson gson = new Gson();
                Perishable perishable = gson.fromJson(getJsonString(httpExchange), Perishable.class);

                if(!perishable.isValidSchema()){
                    sendResponse(httpExchange, "Invalid Schema for perishable product!");
                    return;
                }

                try{
                    eCommerce.insertPerishableItem(perishable.getName(), perishable.getPrice(), perishable.getType(), perishable.getManufacturingDate(), perishable.getExpiryDate());
                    sendResponse(httpExchange, "Product added to Server (DB)");

                } catch (Exception e) {
                    sendResponse(httpExchange, e.toString());
                }

            }
            catch (Exception e){
                sendResponse(httpExchange, e.toString());
            }
        }
    }

    class addNewNonPerishableProduct implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            try {
                Gson gson = new Gson();
                NonPerishable nonPerishable = gson.fromJson(getJsonString(httpExchange), NonPerishable.class);

                if(!nonPerishable.isValidSchema()){
                    sendResponse(httpExchange, "Invalid Schema for non-perishable product!");
                    return;
                }

                try{
                    eCommerce.insertNonPerishableItem(nonPerishable.getName(), nonPerishable.getPrice(), nonPerishable.getType(), nonPerishable.getManufacturingDate());
                    sendResponse(httpExchange, "Product added to Server (DB)");

                } catch (Exception e) {
                    sendResponse(httpExchange, e.toString());
                }

            }
            catch (Exception e){
                sendResponse(httpExchange, e.toString());
            }
        }
    }

    class addPerishableItemToCart implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            try{
                Gson gson = new Gson();
                Integer item_id = gson.fromJson(getJsonString(httpExchange), Integer.class);

                Perishable item = eCommerce.getPerishableItemById(item_id);

                if(!item.isValidSchema()){
                    sendResponse(httpExchange, "Invalid item id for perishable product");
                    return;
                }

                cart.add(item);

                sendResponse(httpExchange, "Item added to cart!");

            } catch (Exception e) {
                sendResponse(httpExchange, e.toString());
            }

        }
    }


    private class addNonPerishableItemToCart implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            try {
                Gson gson = new Gson();
                Integer item_id = gson.fromJson(getJsonString(httpExchange), Integer.class);

                NonPerishable item = eCommerce.getNonPerishableItemById(item_id);
                if(!item.isValidSchema()){
                    sendResponse(httpExchange, "Invalid item id for non-perishable product");
                    return;
                }

                cart.add(item);

                sendResponse(httpExchange, "Item added to cart!");

            } catch (Exception e) {
                sendResponse(httpExchange, e.toString());
            }
        }
    }

    private class GSTOfCart implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            try {
                Float gst = 0.00f;
                for(Item i : cart){
                    gst += record(i);
                }

                sendResponse(httpExchange, "GST is : " + gst);

            } catch (Exception e){
                sendResponse(httpExchange, e.toString());
            }
        }
    }

    Float record(Item i) throws SQLException {
        Integer gst = eCommerce.getGSTById(Integer.parseInt(i.getType()));
        return i.getPrice() * (float) gst / 100;
    }

}
