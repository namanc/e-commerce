package com.e.commerce;

import java.io.FileInputStream;
import java.util.Objects;
import java.util.Properties;

public class Config
{
    Properties configFile;

    public Config(String pathToConfigFile) {
        configFile = new java.util.Properties();
        try {
            configFile.load(new FileInputStream(pathToConfigFile));
        }catch(Exception eta){
            System.out.println("Unable to read Config file..");
            eta.printStackTrace();
        }
    }


    public String getProperty(String key)
    {
        return this.configFile.getProperty(key);
    }
}

