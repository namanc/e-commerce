package com.e.commerce.Schema;

public class NonPerishable implements Item {
    private Integer id;
    private final Integer price;
    private final String name, type, manufacturingDate;
    public final static String tableName = "non_perishable";

    public NonPerishable(Integer id, String name, Integer price, String type, String manufacturingDate) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.type = type;
        this.manufacturingDate = manufacturingDate;
    }

    public NonPerishable(String name, Integer price, String type, String manufacturingDate) {
        this(null, name, price, type, manufacturingDate);
    }

    public NonPerishable(){
        this.id = null;
        this.type = null;
        this.manufacturingDate = null;
        this.price = null;
        this.name = null;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() { return price; }

    public String getType() {
        return type;
    }

    public String getManufacturingDate() {
        return manufacturingDate;
    }


    @Override
    public String insertIntoDb(Integer typeId) {
        return "INSERT IGNORE INTO " + getTableName() + " (name, price, type, manufacturing_date)"
                + " VALUES ('" + name + "', " + price + ", " + typeId + ", '" + manufacturingDate + "');";
    }

    @Override
    public Integer setId(Integer id) {
        this.id = id;

        return this.id;
    }

    @Override
    public String getTableName() {
        return tableName;
    }

    public Boolean isValidSchema() {
        if(name == null || price == null || type == null || manufacturingDate == null)
            return Boolean.FALSE;
        return Boolean.TRUE;
    }
}
