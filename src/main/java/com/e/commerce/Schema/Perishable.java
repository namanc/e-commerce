package com.e.commerce.Schema;

public class Perishable implements Item{
    private Integer id;
    private final String name;
    private final String type;
    private final String manufacturingDate;
    private final String expiryDate;
    private final Integer price;
    public final static String tableName = "perishable";

    public Perishable(Integer id, String name, Integer price, String type, String manufacturingDate, String expiryDate) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.type = type;
        this.manufacturingDate = manufacturingDate;
        this.expiryDate = expiryDate;
    }

    public Perishable(String name, Integer price, String type, String manufacturingDate, String expiryDate) {
        this(null, name, price, type, manufacturingDate, expiryDate);
    }

    public Perishable(){
        this.id = null;
        this.type = null;
        this.manufacturingDate = null;
        this.expiryDate = null;
        this.price = null;
        this.name = null;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getType() {
        return type;
    }

    public String getManufacturingDate() {
        return manufacturingDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    @Override
    public String insertIntoDb(Integer typeId) {
        return "INSERT IGNORE INTO " + getTableName() + " (name, price, type, manufacturing_date, expiry_date) " +
                "VALUES ('" + name + "', " + price + ", " + typeId + ", '" + manufacturingDate +  "', '" + expiryDate + "');";
    }

    @Override
    public Integer setId(Integer id) {
        this.id = id;

        return this.id;
    }

    @Override
    public String getTableName() {
        return tableName;
    }

    public Boolean isValidSchema() {
        if(name == null || price == null || expiryDate == null || type == null || manufacturingDate == null )
            return Boolean.FALSE;
        return Boolean.TRUE;
    }
}
