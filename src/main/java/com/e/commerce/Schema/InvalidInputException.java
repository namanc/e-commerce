package com.e.commerce.Schema;

public class InvalidInputException extends Exception {
    public InvalidInputException(String param, String message){
        super("Invalid Input for : " + param + " Can't be : " +  message);
    }
}
