package com.e.commerce.Schema;

public interface Item {
    String insertIntoDb(Integer typeId);
    String getName();
    Integer setId(Integer id);
    String getType();
    String getTableName();
    Integer getId();
    Integer getPrice();
}
