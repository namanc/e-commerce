package com.e.commerce.Schema;

public class Category {
    private Integer id;
    private final String name;
    private Integer gst;

    public Category(Integer id, String name, Integer gst) throws InvalidInputException {
        setId(id);
        this.name = name;
        setGst(gst);
    }

    public Category(String name, Integer gst) throws InvalidInputException {
        this(-1, name, gst);
    }

    private void setId(Integer id){
        if(id > -1){
            this.id = id;
            return;
        }

        this.id = null;
    }

    private void setGst(Integer gst) throws InvalidInputException{
        if(gst < 0){
            throw new InvalidInputException("GST", "Negative");
        }

        this.gst = gst;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getGst() {
        return gst;
    }
}
