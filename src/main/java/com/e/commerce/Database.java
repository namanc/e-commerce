package com.e.commerce;

import com.e.commerce.Schema.Item;
import com.e.commerce.Schema.NonPerishable;
import com.e.commerce.Schema.Perishable;

import java.sql.*;

public class Database {
    private final String dbName;

    private Statement stmt = null;
    private String categoryTableName;
    private Connection con;

    public Database(Config config) {
        String user = config.getProperty("mDbUser");
        String pass = config.getProperty("mDbPwds");
        this.dbName = config.getProperty("mDbName");
        this.categoryTableName = config.getProperty("mCategoryTableName");
        String url = "jdbc:mysql://localhost:3306";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(url + "/?useSSL=false", user, pass);

            System.out.println("Connecting to database...");
            stmt = con.createStatement();

            System.out.println("Creating database...");
            stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS " + this.dbName + ";");
            con.close();


            con = DriverManager.getConnection(url + "/" + this.dbName + "?useSSL=false", user, pass);
            stmt = con.createStatement();
            System.out.println("Database created successfully");

            System.out.println("Creating tables...");
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS `perishable` (" +
                    "`id` INT NOT NULL AUTO_INCREMENT UNIQUE," +
                    "`name` varchar(255) NOT NULL UNIQUE," +
                    "`price` INT(11) NOT NULL," +
                    "`type` INT(11) NOT NULL," +
                    "`manufacturing_date` DATE NOT NULL," +
                    "`expiry_date` DATE NOT NULL," +
                    "PRIMARY KEY (`id`)" +
                    ");");

            stmt.executeUpdate(
                    "CREATE TABLE IF NOT EXISTS `non_perishable` (" +
                    "`id` INT NOT NULL AUTO_INCREMENT UNIQUE," +
                    "`name` varchar(255) NOT NULL UNIQUE," +
                    "`price` INT(11) NOT NULL," +
                    "`type` INT(11) NOT NULL," +
                    "`manufacturing_date` DATE NOT NULL," +
                    "PRIMARY KEY (`id`)" +
                    ");");

            stmt.executeUpdate(
                    "CREATE TABLE IF NOT EXISTS `category` (" +
                    "`id` INT(11) NOT NULL AUTO_INCREMENT UNIQUE," +
                    "`name` varchar(255) NOT NULL UNIQUE," +
                    "`gst` INT(11) NOT NULL," +
                    "PRIMARY KEY (`id`)" +
                    ");"
            );

            System.out.println("Tables created successfully");

            System.out.println("Adding foreign key...");
            stmt.executeUpdate("ALTER TABLE `perishable` ADD CONSTRAINT `perishable_fk0` FOREIGN KEY (`type`) REFERENCES `category`(`id`);");
            stmt.executeUpdate("ALTER TABLE `non_perishable` ADD CONSTRAINT `non_perishable_fk0` FOREIGN KEY (`type`) REFERENCES `category`(`id`);");
            System.out.println("Foreign key added successfully");

        } catch (Exception e) {

            System.out.println(e.getMessage());
        }

    }

    public String getDbName() {
        return dbName;
    }

    public Boolean insertItem(Item item){
        try{
            String cmd = item.insertIntoDb(insertType(item.getType(), 0));
            System.out.println("Executing command : " + cmd);
            stmt.executeUpdate(cmd);

            ResultSet rs = stmt.executeQuery("SELECT id FROM " + item.getTableName() + " WHERE name = '" + item.getName() + "';");
            rs.next();  // Goto 1st(only available Record.

            item.setId(rs.getInt("id"));
            System.out.println("Output is : " + item.getId());

        } catch (SQLException e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Integer insertType(String type, Integer gst){
        try{
            String cmd = "INSERT IGNORE INTO " + categoryTableName + " (name, gst) VALUES ('" + type + "', " + gst + ");";
            System.out.println("Executing Command : " + cmd);
            stmt.executeUpdate(cmd);

            ResultSet rs = stmt.executeQuery("SELECT id FROM " + categoryTableName + " WHERE name='" + type + "';");
            rs.next();  // Goto 1st(only) available Record.

            return rs.getInt("id");
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public Item getItem(Item item) throws SQLException {
        if(item.getName() == null || item.getTableName() == null){
            return new Item() {
                @Override
                public String insertIntoDb(Integer typeId) {
                    return null;
                }

                @Override
                public String getName() {
                    return null;
                }

                @Override
                public Integer setId(Integer id) {
                    return null;
                }

                @Override
                public String getType() {
                    return null;
                }

                @Override
                public String getTableName() {
                    return null;
                }

                @Override
                public Integer getId() {
                    return null;
                }

                @Override
                public Integer getPrice() {
                    return null;
                }
            };
        }

        ResultSet rs = con.createStatement().executeQuery("Select * from " + item.getTableName() + " where name = '" + item.getName() +"';");

        rs.next();
        String type = getTypeFromId(rs.getInt("type"));

        if(item.getTableName().startsWith("p"))
            return new Perishable(rs.getInt("id"), rs.getString("name"), rs.getInt("price"), type, rs.getString("manufacturing_date"), rs.getString("expiry_date"));

        return new NonPerishable(rs.getInt("id"), rs.getString("name"), rs.getInt("price"), type , rs.getString("manufacturing_date"));
    }

    public String getTypeFromId(Integer id) throws SQLException {
        ResultSet rs = con.createStatement().executeQuery("Select * from " + categoryTableName + " where id = " + id + ";" );
        rs.next();
        return  rs.getString("name");
    }

    public Perishable getPerishableItemById(int id) throws SQLException {
        ResultSet rs = con.createStatement().executeQuery("Select * from " + Perishable.tableName + " where id = " + id + ";");
        rs.next();

        return new Perishable(rs.getInt("id"), rs.getString("name"), rs.getInt("price"), rs.getString("type"), rs.getString("manufacturing_date"), rs.getString("expiry_date"));
    }

    public NonPerishable getNonPerishableItemById(int id) throws SQLException {
        ResultSet rs = con.createStatement().executeQuery("Select * from " + NonPerishable.tableName + " where id = " + id + ";");
        rs.next();

        return new NonPerishable(rs.getInt("id"), rs.getString("name"), rs.getInt("price"), rs.getString("type") , rs.getString("manufacturing_date"));
    }

    public Integer getGSTFromId(Integer id) throws SQLException {
        ResultSet rs = con.createStatement().executeQuery("Select * from " + categoryTableName + " where id = " + id + ";" );
        rs.next();

        return rs.getInt("gst");
    }
}